package com.orc.reader.service;

import com.google.common.collect.Lists;
import com.orc.reader.model.Output;
import com.orc.reader.util.OrcUtil;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.vector.*;
import org.apache.orc.OrcFile;
import org.apache.orc.Reader;
import org.apache.orc.RecordReader;
import org.apache.orc.TypeDescription;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class OrcReaderService {

    private final OrcUtil orcUtil;

    public OrcReaderService(OrcUtil orcUtil) {
        this.orcUtil = orcUtil;
    }

    /**
     * If you generate an orc file using amazon kinesis and you set a column as a partition key,it will not be included in
     * the orc file,so you need to read the columns that are not partition keys.
     *
     * 1) In order to use this tool, copy your orc file into the orc-reader(to be copied in this exact folder, not in subfolder) folder.
     * If you want to use the UI to see the data from the orc file, run this spring boot app, open your browser and enter this url:
     * https://localhost:8080/orc-reader
     * This will bring up the UI.From here,simply select the orc file that you have put in the orc-reader folder.Press 'See Output' button and
     * you should see a Output card with a table containing the data.
     *
     * 2) If you want to see the data as a json, use the restController for that.Run the app, open the browser or PostMan or Insomnia(this is my preferred API platform).
     * Use this url to get the data:
     * https://8080/dynamic-orc-file?filePath=your_file_path_from_orc_reader_app_folder
     *
     * @param filePath
     * @throws IOException
     */
    public Output readOrcFile(String filePath) throws IOException {
        Configuration conf = new Configuration();

        Reader reader = OrcFile.createReader(new Path(filePath), OrcFile.readerOptions(conf));
        RecordReader records = reader.rows(reader.options());
        VectorizedRowBatch batch = reader.getSchema().createRowBatch(2048);

        TypeDescription schema = reader.getSchema();
        List<TypeDescription> columnTypes = schema.getChildren();
        List<String> columns = schema.getFieldNames();
        List<Object> values = new ArrayList<>();

        while (records.nextBatch(batch)) {
            for (int r = 0; r < batch.size; r++) {
                for (int col = 0; col < batch.numCols; col++) {
                    ColumnVector columnVector = batch.cols[col];

                    if (values.size() > 5000) {
                        break;
                    } else {
                        values.add(orcUtil.getValueForColumn(columnTypes.get(col), r, columnVector));
                    }
                }
            }
        }

        records.close();

        return new Output(columns, Lists.partition(values, batch.numCols));
    }

    public List<Pair<String, Object>> readOrcFileRest(String filePath) throws IOException {
        Configuration conf = new Configuration();

        Reader reader = OrcFile.createReader(new Path(filePath), OrcFile.readerOptions(conf));
        RecordReader records = reader.rows(reader.options());
        VectorizedRowBatch batch = reader.getSchema().createRowBatch(2048);

        TypeDescription schema = reader.getSchema();
        List<TypeDescription> columnTypes = schema.getChildren();
        List<String> columns = schema.getFieldNames();
        List<Pair<String, Object>> output = new ArrayList<>();

        while (records.nextBatch(batch)) {
            for (int r = 0; r < batch.size; r++) {
                for (int col = 0; col < batch.numCols; col++) {
                    ColumnVector columnVector = batch.cols[col];
                    output.add(new ImmutablePair<>(columns.get(col), orcUtil.getValueForColumn(columnTypes.get(col), r, columnVector)));
                }
            }
        }

        records.close();

        return output;
    }
}
