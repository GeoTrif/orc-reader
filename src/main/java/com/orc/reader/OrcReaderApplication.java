package com.orc.reader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrcReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrcReaderApplication.class, args);
	}

}
