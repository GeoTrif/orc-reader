package com.orc.reader.controller;

import com.orc.reader.service.OrcReaderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/orc-reader")
public class OrcReaderController {

    private final OrcReaderService orcReaderService;

    public OrcReaderController(OrcReaderService orcReaderService) {
        this.orcReaderService = orcReaderService;
    }

    @GetMapping
    public String getOrcReaderPage(Model model) {
        model.addAttribute("filePath", "");

        return "orc-reader";
    }

    @PostMapping
    public String getOutputPage(@RequestParam String filePath, Model model) throws IOException {
        model.addAttribute("columns", orcReaderService.readOrcFile(filePath).getColumnNames());
        model.addAttribute("values", orcReaderService.readOrcFile(filePath).getValues());

        return "orc-reader-output";
    }
}
