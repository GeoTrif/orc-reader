package com.orc.reader.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Output implements Serializable {

    private List<String> columnNames;
    private List<List<Object>> values;

    public Output() {
    }

    public Output(List<String> columnNames, List<List<Object>> values) {
        this.columnNames = columnNames;
        this.values = values;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public List<List<Object>> getValues() {
        return values;
    }

    public void setValues(List<List<Object>> values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Output output = (Output) o;

        if (!Objects.equals(columnNames, output.columnNames)) return false;
        return Objects.equals(values, output.values);
    }

    @Override
    public int hashCode() {
        int result = columnNames != null ? columnNames.hashCode() : 0;
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Output{" +
                "columnNames=" + columnNames +
                ", values=" + values +
                '}';
    }
}
