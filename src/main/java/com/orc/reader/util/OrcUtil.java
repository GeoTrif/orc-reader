package com.orc.reader.util;

import org.apache.hadoop.hive.ql.exec.vector.*;
import org.apache.orc.TypeDescription;
import org.springframework.stereotype.Component;

@Component
public class OrcUtil {

    public Object getValueForColumn(TypeDescription description, int index, ColumnVector columnVector) {

        switch (description.getCategory().getName()) {
            case "tinyint":
            case "smallint":
            case "int":
            case "bigint":
            case "boolean":
            case "date":
                return ((LongColumnVector) columnVector).vector[index];
            case "string":
            case "varchar":
                return ((BytesColumnVector) columnVector).toString(index);
            case "float":
            case "double":
                return ((DoubleColumnVector) columnVector).vector[index];
            case "decimal":
                return ((DecimalColumnVector) columnVector).vector[index];
            case "timestamp":
                return ((TimestampColumnVector) columnVector).getTimestampAsLong(index);
            case "struct":
                // TODO: Create proper reading of struct types.
                return ((StructColumnVector) columnVector).fields;
        }

        return new Object();
    }
}
