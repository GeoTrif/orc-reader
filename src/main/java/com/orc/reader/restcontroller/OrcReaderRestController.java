package com.orc.reader.restcontroller;

import com.orc.reader.service.OrcReaderService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/orc-reader-rest")
public class OrcReaderRestController {

    private final OrcReaderService orcReaderService;

    public OrcReaderRestController(OrcReaderService orcReaderService) {
        this.orcReaderService = orcReaderService;
    }

    @GetMapping("/dynamic-orc-file")
    public List<Pair<String, Object>> readOrcFile(@RequestParam String filePath) throws IOException {
        return orcReaderService.readOrcFileRest(filePath);
    }
}